# Node backend starter

Simple, beautiful and powerful seed for your next awesome project.

The main purpose of this repository is to provide a good end-to-end project setup and workflow for JavaScript backend development.
I will try to keep this up-to-date, but community contributions and recommendations for improvements are encouraged and will be most welcome.

## Tech Stack
This project is my collection of various technologies I tend to pick for backend development.
It's meant as an entry point to provide faster start.

 1. [Node.js](https://nodejs.org/en/docs/)
 2. [Typescript](https://www.typescriptlang.org/)
 3. [Routing-controllers](https://github.com/typestack/routing-controllers)
 4. [Socket-controllers](https://github.com/typestack/socket-controllers)
 5. [RxJS](https://github.com/Reactive-Extensions/RxJS)
 6. [Swagger](https://swagger.io/)
 7. [Sequelize](docs.sequelizejs.com/)
 8. [Lodash](https://lodash.com/)

Routing-controllers and Socket-controllers are fantastic libraries and core of this collection.
They allow creating controllers using annotations and provide dependency injection capabilities.
Definitely check out documentation.

## Directory Layout

```bash
.
├── /backend/
│   ├── /migrations/                # Database schema migrations
│   ├── /seeds/                     # Scripts with reference/sample data
│   ├── /src/                       # Application source files
│   │   ├── /models/                # Database models (entities)
│   │   ├── /rest-controllers/      # Directory for REST controllers
│   │   ├── /services/              # Services performing various tasks
│   │   ├── /socket-controllers/    # Directory for socket controllers
│   │   └── /Server.js              # Node.js server (entry point
│   ├── /package.json               # List of project dependencies
│   ├── /tsconfig.json              # Basic typescript config
│   └── /tslint.json                # Basic linting definitions
├── /frontend/                      # Put your favorite frontend framework here
└── README.md                       # Project's wiki
```

## How to start

1. Clone repo
2. Install dependencies `yarn` or `npm i`
3. Rename few parameters in `Server.ts` to match your project preferences.
4. Setup database access in `Server.ts`
5. Run server `yarn start` or `npm start`

## Scripts explained

 * `start` - runs server with automatic reload on changes
 * `debug` - same as start but allows for debugging
 * `build` - compile typescript to javascript
 * `prod` - run server after deployment

## Deployment

 I tried several approaches on how to best deploy such application and decided to simply run it via ts-node.
 I also recommend using pm2 which simplifies managing running servers and provides statistics.

