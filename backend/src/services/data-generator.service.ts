import { interval, of } from 'rxjs';
import { ConnectableObservable } from 'rxjs/internal/observable/ConnectableObservable';
import { bufferTime, concatMap, delay, map, share } from 'rxjs/operators';

export default class DataGeneratorService {

  dataGenerator(STREAMS_COUNT: number = 1): ConnectableObservable<any[]> {
    return interval(250)
        .pipe(
            concatMap(x => of(x)
                .pipe(delay(x < 2 ? 0 : 10000 + ((Math.random() - .5) * 100)))),
            map((el: any) => {
              const out: any[] = [];
              for (let i = 0; i < STREAMS_COUNT; i++) {
                for (let j = 0; j < 10; j++) {
                  out.push({
                    x: el,
                    y: (Math.floor(Math.random() * 10)) - 5
                  });
                }
              }
              return out;
            }),
            share()
        ) as ConnectableObservable<any[]>;
  }
}