import Post from '../models/post.model';
import * as Bluebird from 'bluebird';
import { sequelize } from '../Server';

export default class PostsService {
  constructor() {

  }

  getAllPosts(): Bluebird<Post[]> {
    return Post.findAll(); // Sequelize operations
  }

  getOne(): Bluebird<Post> {
    return Post.findOne(); // Sequelize operations
  }

  savePost(postData) {
    // Save post into database
    return "Post " + postData + " !saved!";
  }

  putPost(postData) {
    return "Post #" + postData.id + " has been putted!";
  }

  patchPost(postData) {
    return "Post #" + postData.id + " has been patched!";
  }

  removePost(postData) {
    return "Post #" + postData.id + " has been removed!";
  }

  customQueryExample(query: string) {
    return sequelize.query(query);
  }
}
