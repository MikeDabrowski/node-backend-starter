import { ContentType, Delete, Get, JsonController, QueryParam, Patch, Post, Put, Req, Res } from 'routing-controllers';
import { Request, Response } from 'express';
import PostsService from '../services/posts.service';

// Controller's path prefix may be provided as string parameter of @JsonController annotation
@JsonController()
export class PostController {

  constructor(private postsService: PostsService) {}

  /**
   * @swagger
   * /posts:
   *   get:
   *     tags:
   *       - Rest
   *     summary: Get all posts
   *     description: Get all posts
   *     produces:
   *       - posts array
   *     responses:
   *       200:
   *         description: An array of items
   *       500:
   *         description: Error
   */
  @Get("/posts")
  getAll(@Res() response: Response) {
    return this.postsService.getAllPosts()
        .then((posts) => response.status(200).json(posts))
        .catch((err) => {
          throw new Error(err);
        })
  }

  @Get("/posts/:id")
  getOne() {
    return this.postsService.getAllPosts();
  }

  @Post("/posts")
  post(@Req() request: Request) {
    return this.postsService.savePost(request.body);
  }

  @Put("/posts/:id")
  put(@Req() request: Request) {
    return this.postsService.putPost(request.body);
  }

  @Patch("/posts/:id")
  patch(@Req() request: Request) {
    return this.postsService.patchPost(request.body);
  }

  @Delete("/posts/:id")
  remove(@Req() request: Request) {
    return this.postsService.removePost(request.body);
  }

}
