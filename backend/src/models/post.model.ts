import { Column, Model, Table } from 'sequelize-typescript';

/**
 * @swagger
 * definitions:
 *   CompoundModel:
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 *       description:
 *         type: string
 *       content
 *         type: string
 *       color
 *         type: string
 */
@Table({
  tableName: 'posts',
  timestamps: false
})
export default class Post extends Model<Post> {
  @Column({
    primaryKey: true,
    autoIncrement: true
  })
  compoundId: number;

  @Column
  id: string;

  @Column
  name: string;

  @Column
  description: string;

  @Column
  content: string;

  @Column
  color: string;
}
