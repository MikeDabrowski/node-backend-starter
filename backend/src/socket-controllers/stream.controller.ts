import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';
import { ConnectedSocket, OnConnect, OnDisconnect, SocketController } from 'socket-controllers';
import DataGeneratorService from '../services/data-generator.service';

@SocketController()
export class StreamController {

  private generator$;
  private disconnect$ = new Subject();

  constructor(private dataGeneratorService: DataGeneratorService) {
    this.generator$ = this.dataGeneratorService.dataGenerator(1);
  }

  /**
   * @swagger
   * /connection:
   *   get:
   *     tags:
   *       - Sockets
   *     summary: On client connected
   *     description: Start when client connects
   *     produces:
   *       - socket array
   *     responses:
   *       200:
   *         description: An array of items
   *       500:
   *         description: Error
   */
  @OnConnect()
  connection(@ConnectedSocket() socket: any) {
    console.log('client connected');
    this.generator$
        .pipe(takeUntil(this.disconnect$))
        .subscribe(
            el => socket.emit('server-message', el),
            er => console.error(er),
            () => console.log('unsubscribed')
        );

    this.generator$.connect();
  }

  @OnDisconnect()
  disconnect(@ConnectedSocket() socket: any) {
    this.disconnect$.next(true);
    console.log('client disconnected');
  }

}