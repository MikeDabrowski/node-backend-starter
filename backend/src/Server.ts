import * as sio from 'socket.io';
import 'reflect-metadata';
import { createExpressServer, useContainer } from 'routing-controllers';
import { useSocketServer } from 'socket-controllers';
import { Container } from 'typedi';
import * as swagger from 'swagger-node-express';
import * as http from 'http';
import { Sequelize } from 'sequelize-typescript';
import swaggerUi = require('swagger-ui-express');
import swaggerJSDoc = require('swagger-jsdoc');

// API url will be added as prefix to every route
export const API_URL = '/api';
export const PORT = 2018;
// Project or server name
export const SERVER_NAME = '';

useContainer(Container);

class Server {
  private _port = PORT;

  constructor() {
    this.configSequelize();
    this.createServer();
    this.configSwagger();
    this.setupPolyfills();
    this._app = http.createServer(this._app);
    this.createSocket();
    this._app.listen(this._port, () => {
      console.log('Server started on port ' + this._port);
    });
  }

  private _sequelize: Sequelize;

  get sequelize() {
    return this._sequelize;
  }

  private _app;

  get app() {
    return this._app;
  }

  private _io;

  get io() {
    return this._io;
  }

  public static bootstrap(): Server {
    return new Server();
  }

  createServer() {
    const options = {
      routePrefix: API_URL,
      defaults: {
        nullResultCode: 404,
        undefinedResultCode: 204,
        paramOptions: {
          required: false, // with this option, argument will be required by default
        },
      },
      classTransformer: false,
      cors: true,
      defaultErrorHandler: true,
      controllers: [__dirname + '/rest-controllers/*.controller.ts'],
    };
    this._app = createExpressServer(options);
  }

  configSequelize() {
    this._sequelize = new Sequelize({
      database: 'database',
      username: 'username',
      password: 'password',
      host: 'host',
      dialect: 'mysql',
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      },
      modelPaths: [__dirname + '/models/*.model.ts']
    });
  }

  configSwagger() {
    swagger.createNew(this._app);
    const options = {
      swaggerDefinition: {
        info: {
          title: `${SERVER_NAME} API`,
          version: '1.0.0',
          description: `REST API reference for ${SERVER_NAME}`,
        },
        host: `localhost:${this._port}`,
        basePath: API_URL
      },
      apis: [
        __dirname + '/rest-controllers/*.ts',
        __dirname + '/socket-controllers/*.ts',
        __dirname + '/models/*.ts',
        __dirname + '/middlewares/*.ts'
      ],
      swaggerUrl: `http://localhost:${this._port}/swagger.json`
    };
    const swaggerSpec = swaggerJSDoc(options);
    this._app.get('/swagger.json', (req, res) => {
      res.setHeader('Content-Type', 'application/json');
      res.send(swaggerSpec);
    });
    this._app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, options));
  }

  private createSocket() {
    const ioo = sio(this._app, {
      serveClient: false,
      wsEngine: 'ws', // uws is not supported since it is a native module
    });
    useSocketServer(ioo, {
      controllers: [__dirname + '/socket-controllers/*.ts']
    });
  }

  private setupPolyfills() {
  }
}

const server = Container.get(Server);
export const sequelize = server.sequelize;
export const app = server.app;
export const io = server.io;
